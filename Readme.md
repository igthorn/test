## Igthorn Test

[![Igthorn](http://igthorn.com/assets/images/igthorn_resized.png "Igthorn")](http://igthorn.com "Igthorn")

RethinkDB Test provides helpers to load fixtures, data providers and more.

[![Build Status](https://gitlab.com/igthorn/test/badges/master/build.svg)](https://gitlab.com/igthorn/test/builds)

RethinkDB Test provides helpers to load fixtures, data providers and more. It's part of [Igthorn](http://igthorn.com "Igthorn") project

### Usage

Please refer to [unit tests](https://gitlab.com/igthorn/test/blob/master/tests/cases/testHelperTest.js "unit tests") of this module - they contain sample use cases.

### Methods

Test class implements following methods:

* constructor(testPath:string) : self

  sets test file's path
* executeFixtureScript(fileName, callback) : void

  executes passed fixtures script file and calls callback function (for example mocha's done)
* getDataProvider(fileName) : mixed

  requires data provider file and returns it's exported data
* setTestPath(testPath:string) : self

  sets test file's path
* getTestPath() : string

  gets test file's path
* setTestFixturesDirPath(testFixturesDirPath:string) : self

  sets test's fixtures directory path
* getTestFixturesDirPath() : string

  gets test's fixtures directory path
* setTestDataProvidersDirPath(testDataProvidersDirPath:string) : self

  sets test's data providers directory path
* getTestDataProvidersDirPath() : string

  gets test's data providers directory path  

## License

[MIT](https://gitlab.com/igthorn/test/blob/master/license.md)
