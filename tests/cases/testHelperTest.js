
"use strict";

const appRootPath = require("app-root-path");
const _           = require("underscore");
const r           = require(appRootPath + "/tests/mocks/rethinkDbDataProvider");

const assert  = require("assert");

const TestHelper = require(appRootPath + "/lib/testHelper");
const testHelper = new TestHelper(__filename);

describe("TestHelper Test", function() {

  describe("When calling getDataProvider() function passing existing file as argument", function() {

    it("should return data provided by DataProvider", function() {

      const resultsA = testHelper.getDataProvider("sampleA");

      const expectedResultsA = [
        {
          id: "f65a5fe6-371a-4216-884a-ab61047ab3db",
          name: "Something A"
        },
        {
          id: "c9a67fa1-9903-4bb7-a584-31ea0abdcdb6",
          name: "Something B"
        },
        {
          id: "382f0878-29fd-4161-8231-fc931741e49f",
          name: "Something C"
        },
        {
          id: "647c3c3c-309d-476a-8891-c2d569ba14e1",
          name: "Something D"
        },
        {
          id: "61ad71eb-0ca2-49af-b371-c16599207bbc",
          name: "Something E"
        }
      ];

      assert.deepEqual(resultsA, expectedResultsA);

      const resultsB = testHelper.getDataProvider("sampleB");

      const expectedResultsB = [
        {
          id: "f65a5fe6-371a-4216-884a-ab61047ab3db",
          name: "Something X"
        },
        {
          id: "c9a67fa1-9903-4bb7-a584-31ea0abdcdb6",
          name: "Something Y"
        },
        {
          id: "382f0878-29fd-4161-8231-fc931741e49f",
          name: "Something Z"
        }
      ];

      assert.deepEqual(resultsB, expectedResultsB);

    });

  });

  describe("When calling getDataProvider() function passing non-existing file as argument", function() {

    it("should return data provided by DataProvider", function() {

      assert.throws(function() {
        testHelper.getDataProvider("nonExisting");
      });

    });

  });

  describe("When calling getDataProvider() function without argument", function() {

    it("should return data provided by DataProvider", function() {

      assert.throws(function() {
        testHelper.getDataProvider();
      });

    });

  });

  describe("When calling getDataProvider() function with invalid argument", function() {

    it("should return data provided by DataProvider", function() {

      assert.throws(function() {
        testHelper.getDataProvider(null);
      });

      assert.throws(function() {
        testHelper.getDataProvider(false);
      });

      assert.throws(function() {
        testHelper.getDataProvider(true);
      });

      assert.throws(function() {
        testHelper.getDataProvider(undefined);
      });

      assert.throws(function() {
        testHelper.getDataProvider([]);
      });

      assert.throws(function() {
        testHelper.getDataProvider({});
      });

      class A {};
      const a = new A();

      assert.throws(function() {
        testHelper.getDataProvider(a);
      });

    });

  });

  describe("When calling getTestPath() function", function() {

    it("should return tests' path", function() {

      assert.equal(testHelper.getTestPath(), appRootPath + "/tests/cases/testHelperTest.js");

    });

  });

  describe("When calling getTestDataProvidersDirPath() function", function() {

    it("should return tests' path", function() {

      assert.equal(testHelper.getTestDataProvidersDirPath(), appRootPath + "/tests/dataProviders/testHelperTest/");

    });

  });

  describe("When calling executeFixtureScript() function", function() {

    it("should allow to execute fixtures", function(done) {

      testHelper.executeFixtureScript("sampleRethinkDbFixture", function() {
        r.table('sample').run()
          .then(function(documents) {

            const expectedResults = [
              {
                a: "abc",
                b: "def"
              },
              {
                c: "ghi",
                d: "jkl"
              }
            ];

            assert.deepEqual(documents, expectedResults);
            done();
          })
      });

    });

  });

  describe("When calling getTestFixturesDirPath() function", function() {

    it("should return tests' fixtures path", function() {

      assert.equal(testHelper.getTestFixturesDirPath(), appRootPath + "/tests/fixtures/testHelperTest/");

    });

  });

});
