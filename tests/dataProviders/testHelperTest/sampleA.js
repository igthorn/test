
"use strict";

var entity1 =       {
  id: "f65a5fe6-371a-4216-884a-ab61047ab3db",
  name: "Something A"
};

var entity2 = {
  id: "c9a67fa1-9903-4bb7-a584-31ea0abdcdb6",
  name: "Something B"
};

var entity3 = {
  id: "382f0878-29fd-4161-8231-fc931741e49f",
  name: "Something C"
};

var entity4 = {
  id: "647c3c3c-309d-476a-8891-c2d569ba14e1",
  name: "Something D"
};

var entity5 = {
  id: "61ad71eb-0ca2-49af-b371-c16599207bbc",
  name: "Something E"
};

module.exports = [
  entity1,
  entity2,
  entity3,
  entity4,
  entity5
];
