
"use strict";

var entity1 =       {
  id: "f65a5fe6-371a-4216-884a-ab61047ab3db",
  name: "Something X"
};

var entity2 = {
  id: "c9a67fa1-9903-4bb7-a584-31ea0abdcdb6",
  name: "Something Y"
};

var entity3 = {
  id: "382f0878-29fd-4161-8231-fc931741e49f",
  name: "Something Z"
};

module.exports = [
  entity1,
  entity2,
  entity3
];
