
"use strict";

var r = require("./../../mocks/rethinkDbDataProvider");

module.exports = function() {

	return r.tableCreate('sample').run()
	  .then(function() {
	  	return r.table('sample').insert([{a: "abc", b: "def"}, {c: "ghi", d: "jkl"}]);
	  })
};
