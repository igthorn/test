
const r = require('rethinkdbdash')({
  port: 28015,
  host: 'rethinkdb',
  db: 'test'
});

module.exports = r;